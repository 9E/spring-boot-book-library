package com.example.springbootbooklibrary.configs;

public class Configs {
    public static final String PRODUCTION_DATABASE = "databases/books.json";
    public static final String TEST_DATABASE = "databases/testDatabase.json";
}
