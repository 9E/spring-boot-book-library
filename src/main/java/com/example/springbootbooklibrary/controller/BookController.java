package com.example.springbootbooklibrary.controller;

import com.example.springbootbooklibrary.model.Book;
import com.example.springbootbooklibrary.model.ReservationInfo;
import com.example.springbootbooklibrary.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/books")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public List<Book> getBooks() {
        return bookService.getBooks();
    }

    @GetMapping("/search")
    public List<Book> getBooks(@RequestParam Map<String, String> params) {
        return bookService.getFilteredBooks(params);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> getBook(@PathVariable Long id) {
        Book book = bookService.getBookById(id);
        if (book == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    @GetMapping("/guid={guid}")
    public Book getBook(@PathVariable UUID guid) {
        return bookService.getBookByGuid(guid);
    }

    @PostMapping
    public ResponseEntity<Book> addBook(@RequestBody Book book) {
        book.setReservationInfo(null);
        return new ResponseEntity<>(bookService.addBook(book), HttpStatus.CREATED);
    }

    @PutMapping("{id}:take")
    public ResponseEntity<Book> takeBook(@PathVariable(name = "id") Long bookId,
                                         @RequestBody ReservationInfo reservationInfo) {
        Book book = bookService.getBookById(bookId);
        if (book == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (!bookService.canTakeBook(reservationInfo)) {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        book.setReservationInfo(reservationInfo);

        return new ResponseEntity<>(bookService.updateBook(bookId, book), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Book> deleteBook(@PathVariable Long id) {
        Book book = bookService.getBookById(id);
        if (book == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(bookService.deleteBook(id), HttpStatus.OK);
    }
}
