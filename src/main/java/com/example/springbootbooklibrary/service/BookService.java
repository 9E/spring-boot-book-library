package com.example.springbootbooklibrary.service;

import com.example.springbootbooklibrary.model.Book;
import com.example.springbootbooklibrary.model.ReservationInfo;
import com.example.springbootbooklibrary.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Period;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class BookService {

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> getFilteredBooks(Map<String, String> params) {
        List<Predicate<Book>> predicates = getPredicates(params);

        if (!predicates.isEmpty()) {
            Predicate<Book> filterPredicate = predicates.stream().reduce(predicate -> true, Predicate::and);

            return getBooks().stream()
                    .filter(filterPredicate)
                    .collect(Collectors.toList());
        }

        return getBooks();
    }

    private List<Predicate<Book>> getPredicates(Map<String, String> params) {
        String author = params.get("author");
        String category = params.get("category");
        String language = params.get("language");
        String isbn = params.get("isbn");
        String name = params.get("name");
        String isTakenString = params.get("isTaken");

        List<Predicate<Book>> predicates = new ArrayList<>();

        if (author != null) {
            predicates.add(book -> book.getAuthor().contains(author));
        }
        if (category != null) {
            predicates.add(book -> book.getCategory().contains(category));
        }
        if (language != null) {
            predicates.add(book -> book.getLanguage().contains(language));
        }
        if (isbn != null) {
            predicates.add(book -> book.getIsbn().contains(isbn));
        }
        if (name != null) {
            predicates.add(book -> book.getName().contains(name));
        }
        if (isTakenString != null) {
            Boolean isTaken = Objects.equals(isTakenString, "true");
            predicates.add(book -> Objects.equals(book.isTaken(), isTaken));
        }
        return predicates;
    }

    public Book getBookById(Long id) {
        return bookRepository.findById(id);
    }

    public Book getBookByGuid(UUID guid) {
        return bookRepository.findByGuid(guid);
    }

    public Book addBook(Book book) {
        return bookRepository.create(book);
    }

    public Book updateBook(Long id, Book book) {
        return bookRepository.update(id, book);
    }

    public Book deleteBook(Long id) {
        return bookRepository.delete(id);
    }

    public boolean canTakeBook(ReservationInfo reservationInfo) {
        return monthsPeriodIsValid(reservationInfo)
                && !personWillExceedBookLimit(reservationInfo.getTakenBy());
    }

    public boolean monthsPeriodIsValid(ReservationInfo reservationInfo) {
        int maxPermittedMonths = 2;
        int monthsTaken = Period.between(reservationInfo.getTakenFrom(), reservationInfo.getTakenUntil())
                .getMonths();

        return monthsTaken <= maxPermittedMonths;
    }

    public boolean personWillExceedBookLimit(String personName) {
        long maxPermittedBooks = 3;
        long booksAlreadyReserved = getBooks().stream()
                .filter(book -> book.isTaken())
                .filter(book -> book.isTakenBy(personName))
                .count();

        return booksAlreadyReserved >= maxPermittedBooks;
    }

    public List<Book> getBooks() {
        return this.bookRepository.findAll();
    }
}
