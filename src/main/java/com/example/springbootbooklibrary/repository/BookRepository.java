package com.example.springbootbooklibrary.repository;

import com.example.springbootbooklibrary.configs.Configs;
import com.example.springbootbooklibrary.model.Book;
import com.example.springbootbooklibrary.model.ReservationInfo;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Repository
public class BookRepository {

    private final BookDataUtil bookDataUtil;

    public BookRepository() {
        this.bookDataUtil = new BookDataUtil(Configs.PRODUCTION_DATABASE);
    }

    public BookRepository(String storageName) {
        this.bookDataUtil = new BookDataUtil(storageName);
    }

    public Book update(Long id, Book book) {
        List<Book> books = findAll();
        Book bookToUpdate = findInBookListById(books, id);

        bookToUpdate.setAuthor(book.getAuthor());
        bookToUpdate.setName(book.getName());
        bookToUpdate.setAuthor(book.getAuthor());
        bookToUpdate.setCategory(book.getCategory());
        bookToUpdate.setLanguage(book.getLanguage());
        bookToUpdate.setPublicationDate(book.getPublicationDate());
        bookToUpdate.setIsbn(book.getIsbn());
        bookToUpdate.setGuid(book.getGuid());
        bookToUpdate.setReservationInfo(new ReservationInfo(book.getReservationInfo()));

        bookDataUtil.saveBooksToFile(books);
        return bookToUpdate;
    }

    public Book delete(Long id) {
        List<Book> books = findAll();
        Book bookToRemove = findInBookListById(books, id);
        books.remove(bookToRemove);

        bookDataUtil.saveBooksToFile(books);
        return bookToRemove;
    }

    public void deleteAll() {
        bookDataUtil.saveBooksToFile(new ArrayList<>());
    }

    public Book create(Book book) {
        List<Book> books = findAll();
        book.setId(generateId());
        books.add(book);

        bookDataUtil.saveBooksToFile(books);
        return book;
    }

    private Long generateId() {
        List<Book> books = findAll();

        if (books.isEmpty()) {
            return 1L;
        }

        Book bookWithMaxId = books.stream()
                .max((book1, book2) -> Long.compare(book1.getId(), book2.getId()))
                .get();

        return bookWithMaxId.getId() + 1;
    }

    public Book findByGuid(UUID guid) {
        return findAll().stream()
                .filter(book -> Objects.equals(book.getGuid(), guid))
                .findFirst()
                .orElse(null);
    }

    public Book findById(Long id) {
        return findInBookListById(findAll(), id);
    }

    private Book findInBookListById(List<Book> books, Long id) {
        return books.stream()
                .filter(book -> Objects.equals(book.getId(), id))
                .findFirst()
                .orElse(null);
    }

    public List<Book> findAll() {
        return bookDataUtil.loadBooksFromFile();
    }
}
