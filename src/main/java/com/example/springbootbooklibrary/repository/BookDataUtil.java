package com.example.springbootbooklibrary.repository;

import com.example.springbootbooklibrary.model.Book;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BookDataUtil {

    private String storageName;
    private ObjectMapper objectMapper;

    public BookDataUtil(String storageName) {
        this.storageName = storageName;
        this.objectMapper = new ObjectMapper();

        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    public List<Book> loadBooksFromFile() {
        try {
            String jsonString = this.readFile(this.storageName);
            return parseJsonArrayString(jsonString);
        } catch (FileNotFoundException | JsonProcessingException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public List<Book> parseJsonArrayString(String jsonString) throws JsonProcessingException {
        return new ArrayList<>(Arrays.asList(this.objectMapper.readValue(jsonString, Book[].class)));
    }

    public String readFile(String filename) throws FileNotFoundException {
        File file = new File(storageName);
        Scanner scanner = new Scanner(file);
        String content = "";

        while (scanner.hasNextLine()) {
            content += scanner.nextLine();
        }
        scanner.close();

        return content;
    }

    public void saveBooksToFile(List<Book> books) {
        try {
            String jsonString = this.objectMapper.writeValueAsString(books);
            FileWriter writer = new FileWriter(this.storageName);
            writer.write(jsonString);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
