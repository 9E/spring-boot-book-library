package com.example.springbootbooklibrary.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

public class Book {
    private Long id;
    private String name;
    private String author;
    private String category;
    private String language;
    private LocalDate publicationDate;
    private String isbn;
    private UUID guid;
    private ReservationInfo reservationInfo;

    public Book() {
    }

    public Book(Long id, String name, String author, String category, String language, LocalDate publicationDate, String isbn, UUID guid, ReservationInfo reservationInfo) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.category = category;
        this.language = language;
        this.publicationDate = publicationDate;
        this.isbn = isbn;
        this.guid = guid;
        this.reservationInfo = reservationInfo;
    }

    public Book(Book book) {
        this.id = book.getId();
        this.name = book.getName();
        this.author = book.getAuthor();
        this.category = book.getCategory();
        this.language = book.getLanguage();
        this.publicationDate = book.getPublicationDate();
        this.isbn = book.getIsbn();
        this.guid = book.getGuid();
        this.reservationInfo = new ReservationInfo(book.getReservationInfo());
    }

    public Book(String name) {
        this.name = name;
    }

    public Boolean isTakenBy(String personName) {
        if (this.reservationInfo == null) {
            return false;
        }

        return Objects.equals(this.reservationInfo.getTakenBy(), personName);
    }

    @JsonIgnore
    public Boolean isTaken() {
        return this.reservationInfo != null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public LocalDate getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(LocalDate publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public UUID getGuid() {
        return guid;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public ReservationInfo getReservationInfo() {
        return reservationInfo;
    }

    public void setReservationInfo(ReservationInfo reservationInfo) {
        this.reservationInfo = reservationInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return Objects.equals(id, book.id)
                && Objects.equals(name, book.name)
                && Objects.equals(author, book.author)
                && Objects.equals(category, book.category)
                && Objects.equals(language, book.language)
                && Objects.equals(publicationDate, book.publicationDate)
                && Objects.equals(isbn, book.isbn)
                && Objects.equals(guid, book.guid)
                && Objects.equals(reservationInfo, book.reservationInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, author, category, language, publicationDate, isbn, guid, reservationInfo);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", category='" + category + '\'' +
                ", language='" + language + '\'' +
                ", publicationDate=" + publicationDate +
                ", isbn='" + isbn + '\'' +
                ", guid=" + guid +
                ", reservationInfo=" + reservationInfo +
                '}';
    }
}
