package com.example.springbootbooklibrary.model;

import java.time.LocalDate;

public class ReservationInfo {
    private String takenBy;
    private LocalDate takenFrom;
    private LocalDate takenUntil;

    public ReservationInfo() {
    }

    public ReservationInfo(ReservationInfo reservationInfo) {
        this.takenBy = reservationInfo.getTakenBy();
        this.takenFrom = reservationInfo.getTakenFrom();
        this.takenUntil = reservationInfo.getTakenUntil();
    }

    public ReservationInfo(String takenBy, LocalDate takenFrom, LocalDate takenUntil) {
        this.takenBy = takenBy;
        this.takenFrom = takenFrom;
        this.takenUntil = takenUntil;
    }

    public String getTakenBy() {
        return takenBy;
    }

    public void setTakenBy(String takenBy) {
        this.takenBy = takenBy;
    }

    public LocalDate getTakenFrom() {
        return takenFrom;
    }

    public void setTakenFrom(LocalDate takenFrom) {
        this.takenFrom = takenFrom;
    }

    public LocalDate getTakenUntil() {
        return takenUntil;
    }

    public void setTakenUntil(LocalDate takenUntil) {
        this.takenUntil = takenUntil;
    }

    @Override
    public String toString() {
        return "ReservationInfo{" +
                "takenBy='" + takenBy + '\'' +
                ", takenFrom=" + takenFrom +
                ", takenUntil=" + takenUntil +
                '}';
    }
}
