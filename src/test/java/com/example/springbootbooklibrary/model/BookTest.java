package com.example.springbootbooklibrary.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookTest {

    @Test
    void testIsTakenBy() {
        Book book = new Book();
        ReservationInfo reservationInfo = new ReservationInfo("George", null, null);

        assertFalse(book.isTakenBy(reservationInfo.getTakenBy()));

        book.setReservationInfo(reservationInfo);
        assertTrue(book.isTakenBy(reservationInfo.getTakenBy()));
    }

    @Test
    void testIsTaken() {
        Book book = new Book();

        assertFalse(book.isTaken());

        book.setReservationInfo(new ReservationInfo());
        assertTrue(book.isTaken());
    }
}