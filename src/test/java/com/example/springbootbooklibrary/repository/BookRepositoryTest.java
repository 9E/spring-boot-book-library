package com.example.springbootbooklibrary.repository;

import com.example.springbootbooklibrary.configs.Configs;
import com.example.springbootbooklibrary.model.Book;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class BookRepositoryTest {

    private final BookRepository bookRepository = new BookRepository(Configs.TEST_DATABASE);

    @BeforeEach
    void clearTheDatabase() {
        bookRepository.deleteAll();
    }

    @AfterAll
    static void clearDatabaseAfterAll() {
        new BookRepository(Configs.TEST_DATABASE).deleteAll();
    }

    @Test
    void testAddBook() {
        Book book1 = new Book("Book one");
        Book book2 = new Book("Book two");

        this.bookRepository.create(book1);
        this.bookRepository.create(book2);

        assertEquals(this.bookRepository.findAll().size(), 2);
        assertEquals(book2.getId(), 2L);
        assertNotEquals(book1.getId(), 0L);
    }

    @Test
    void testGetBookById() {
        Book book1 = new Book("Book one");
        Book book2 = new Book("Book two");
        Book book3 = new Book("Book three");

        this.bookRepository.create(book1);
        this.bookRepository.create(book2);
        this.bookRepository.create(book3);

        assertEquals(this.bookRepository.findById(3L).getName(), book3.getName());
        assertNull(this.bookRepository.findById(55L));
        assertDoesNotThrow(() -> this.bookRepository.findById(null));
    }

    @Test
    void testGetBookByGuid() {
        UUID uuid1 = UUID.fromString("4bae01fb-ed07-469d-b22a-e500a11083b9");
        Book book1 = new Book(null, "Book one", null, null, null, null, null, uuid1, null);
        UUID uuid2 = UUID.fromString("d19feefa-73ac-4ce6-a738-39ed262ae7e3");
        Book book2 = new Book(null, "Book one", null, null, null, null, null, uuid2, null);
        UUID uuid3 = UUID.fromString("25aab82e-bb0a-11eb-8529-0242ac130003");

        this.bookRepository.create(book1);
        this.bookRepository.create(book2);

        assertEquals(book1, this.bookRepository.findByGuid(uuid1));
        assertNull(this.bookRepository.findByGuid(uuid3));
        assertDoesNotThrow(() -> this.bookRepository.findByGuid(null));
    }
}