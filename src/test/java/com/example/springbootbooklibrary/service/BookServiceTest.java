package com.example.springbootbooklibrary.service;

import com.example.springbootbooklibrary.model.Book;
import com.example.springbootbooklibrary.model.ReservationInfo;
import com.example.springbootbooklibrary.repository.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class BookServiceTest {

    @Mock
    private BookRepository bookRepository;
    private BookService bookService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.bookService = new BookService(bookRepository);
    }

    @Test
    void testMonthsPeriodIsValid() {
        LocalDate date1 = LocalDate.of(2021, 1, 1);
        LocalDate date2 = LocalDate.of(2021, 3, 1);
        LocalDate date3 = LocalDate.of(2021, 4, 1);
        ReservationInfo reservationInfo1 = new ReservationInfo(null, date1, date2);
        ReservationInfo reservationInfo2 = new ReservationInfo(null, date1, date3);
        ReservationInfo reservationInfo3 = new ReservationInfo(null, date1, null);

        assertTrue(this.bookService.monthsPeriodIsValid(reservationInfo1));
        assertFalse(this.bookService.monthsPeriodIsValid(reservationInfo2));
        assertThrows(NullPointerException.class, () -> this.bookService.monthsPeriodIsValid(reservationInfo3));
    }

    @Test
    void testPersonExceedsBookLimit() {
        String reserverName = "Ben";
        ReservationInfo reservationInfo = new ReservationInfo(reserverName, null, null);
        Book book = new Book(null, null, null, null, null, null, null, null, reservationInfo);

        Mockito.when(bookRepository.findAll()).thenReturn(new ArrayList<>());
        assertFalse(bookService.personWillExceedBookLimit(reserverName));

        Mockito.when(bookRepository.findAll()).thenReturn(List.of(new Book(book), new Book(book)));
        assertFalse(bookService.personWillExceedBookLimit(reserverName));

        Mockito.when(bookRepository.findAll()).thenReturn(List.of(new Book(book), new Book(book), new Book(book)));
        assertTrue(bookService.personWillExceedBookLimit(reserverName));
    }

    @Test
    void testGetFilteredBooks() {
        Book book1 = new Book(null, "Toads And Rabbits", null, "Mystery", null, null, null, null, null);
        Book book2 = new Book(null, "Cleaning With My Village", null, "Science Fiction", null, null, null, null, null);
        Book book3 = new Book(null, "Dogs And Lions", null, "Drama", null, null, null, null, null);
        Map<String, String> filterParams = new HashMap<>();

        Mockito.when(bookRepository.findAll()).thenReturn(List.of(book1, book2, book3));

        filterParams.put("name", "o");
        List<Book> filteredBooks = bookService.getFilteredBooks(filterParams);
        assertEquals(2, filteredBooks.size());
        assertTrue(filteredBooks.containsAll(List.of(book1, book3)));

        filterParams.put("category", "a");
        filteredBooks = bookService.getFilteredBooks(filterParams);
        assertEquals(1, filteredBooks.size());
        assertTrue(filteredBooks.contains(book3));
    }
}